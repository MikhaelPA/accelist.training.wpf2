﻿using LiteDB;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WpfTraining2.Models;

namespace WpfTraining2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //private List<UserViewModel> dataUsers = new List<UserViewModel>();

        private static AppConfigModel EnvironmentWatcher = new AppConfigModel();

        private static IHttpClientFactory httpClientFactory;
        public MainWindow()
        {
            //Task.Run(() => TulisFileTxtAsync("Coba.txt"));
            Task.Run(() => BacaFileTxtAsync("Coba.txt"));

            ReadAppConfig();
            RegisterService();
            InitializeComponent();
        }

        /// <summary>
        /// Register Services.
        /// </summary>
        private void RegisterService()
        {
            var services = new ServiceCollection();
            services.AddHttpClient();
            var di = services.BuildServiceProvider();
            httpClientFactory = di.GetRequiredService<IHttpClientFactory>();
        }

        //new
        private void ReadAppConfig()
        {
            // take Connection string
            var conns = ConfigurationManager.ConnectionStrings;

            EnvironmentWatcher.LiteDb = conns["LiteDb"].ToString();

            var appsettings = ConfigurationManager.AppSettings;

            //foreach (var app in appsettings.AllKeys)
            //{
            //    var a = appsettings[app];
            //}

            EnvironmentWatcher.TableUser = appsettings["TableUser"];
            EnvironmentWatcher.ApiGetUser = appsettings["ApiGetUser"];
            EnvironmentWatcher.ApiPostUser = appsettings["ApiPostUser"];
        }

        /// <summary>
        /// Ketika awal datagrid di load/mulai.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void DataGridUser_Loaded(object sender, RoutedEventArgs e)
        {
            dataGridUser.ItemsSource = await LoadDataAsync();
        }

        private async Task<List<UserViewModel>> LoadDataAsync()
        {
            //using (var db = new LiteDatabase(EnvironmentWatcher.LiteDb))
            //{
            //    var dataUsers = db.GetCollection<UserViewModel>(EnvironmentWatcher.TableUser);


            //    dataGridUser.ItemsSource = dataUsers.FindAll();
            //}

            //using System.Net.Http;
            //var client = new HttpClient(); // HARAM
            var client = httpClientFactory.CreateClient();

            var response = await client.GetAsync(EnvironmentWatcher.ApiGetUser);

            response.EnsureSuccessStatusCode();

            var contentJson = await response.Content.ReadAsStringAsync();
            var content = JsonConvert.DeserializeObject<List<UserViewModel>>(contentJson);
            //dataGridUser.ItemsSource = content;
            return content;

        }

        /// <summary>
        /// Ketika tombol daftar ditekan maka masukkan data input kedalam grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void BtnDaftar_Click(object sender, RoutedEventArgs e)
        {
            //using (var db = new LiteDatabase(EnvironmentWatcher.LiteDb))
            //{
            //    var tableUser = db.GetCollection<UserViewModel>(EnvironmentWatcher.TableUser);

            //    var dataUser = new UserViewModel()
            //    {
            //        Name = textBoxNama.Text,
            //        Email = textBoxEmail.Text,
            //        Git = textBoxGit.Text,
            //        Gender = (bool)radioMale.IsChecked ? "Male" : "Female"
            //    };

            //    tableUser.Insert(dataUser);
            //    tableUser.EnsureIndex(Q => Q.Name);
            //    dataGridUser.ItemsSource = tableUser.FindAll();
            //}
            btnDaftar.IsEnabled = false;
            var client = httpClientFactory.CreateClient();//new HttpClient();
            var dataUser = new UserNewModel()
            {
                Name = textBoxNama.Text,
                Email = textBoxEmail.Text,
                Git = textBoxGit.Text,
                Gender = (bool)radioMale.IsChecked ? false : true
            };
            var json = new StringContent(JsonConvert.SerializeObject(dataUser), Encoding.UTF8, "application/json");

            var response = await client.PostAsync(EnvironmentWatcher.ApiPostUser, json);

            try
            {
                response.EnsureSuccessStatusCode();
                dataGridUser.ItemsSource = await LoadDataAsync();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }

            ResetFieldInput();
            btnDaftar.IsEnabled = true;
        }

        private void ResetFieldInput()
        {
            textBoxNama.Text = "";
            textBoxEmail.Text = "";
            textBoxGit.Text = "";
            radioMale.IsChecked = true;
        }

        /// <summary>
        /// Button untuk mencari data dari grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void BtnSearch_Click(object sender, RoutedEventArgs e)
        {
            //using (var db = new LiteDatabase(EnvironmentWatcher.LiteDb))
            //{
            //    // penampung collection dari lite db
            //    var dataUserFiltered = db.GetCollection<UserViewModel>(EnvironmentWatcher.TableUser);


            //    // Cek kalau kosong
            //    if (string.IsNullOrEmpty(textBoxSearch.Text))
            //    {
            //        dataGridUser.ItemsSource = dataUserFiltered.FindAll();
            //        return;
            //    }
            //    dataGridUser.ItemsSource = dataUserFiltered.Find(Q => Q.Name.Contains(textBoxSearch.Text));

            //}

            dataGridUser.ItemsSource = (await LoadDataAsync()).Where(Q => Q.Name.Contains(textBoxSearch.Text) || Q.Name.Contains(textBoxEmail.Text));
        }

        private async Task BacaFileTxtAsync(string namaFile)
        {
            if (string.IsNullOrEmpty(namaFile))
            {
                MessageBox.Show("Nama File Tidak boleh kosong");
                return;
            }
            if (!File.Exists(namaFile))
            {
                MessageBox.Show("File Tidak ditemukan");
                return;
            }

            //MessageBox.Show(File.ReadAllText(namaFile));
            //byte[] buffer;
            ////System.IO
            //using (var stream = new FileStream(namaFile, System.IO.FileMode.OpenOrCreate,FileAccess.Read))
            //{
            //    buffer = new byte[stream.Length];
            //    await stream.ReadAsync(buffer, 0, buffer.Length);

            //}
            //MessageBox.Show(System.Text.Encoding.ASCII.GetString(buffer));
            MessageBox.Show(await ReadTextAsync(namaFile));
        }

        private async Task<string> ReadTextAsync(string filePath)
        {
            using (var sourceStream = new FileStream(filePath,
                System.IO.FileMode.Open, FileAccess.Read, FileShare.Read,
                bufferSize: 4096, useAsync: true))
            {
                var sb = new StringBuilder();

                var buffer = new byte[0x1000];
                int numRead;
                while ((numRead = await sourceStream.ReadAsync(buffer, 0, buffer.Length)) != 0)
                {
                    var text = Encoding.Unicode.GetString(buffer, 0, numRead);
                    sb.Append(text);
                }

                return sb.ToString();
            }
        }

        private async Task TulisFileTxtAsync(string namaFile)
        {
            if (string.IsNullOrEmpty(namaFile))
            {
                MessageBox.Show("Nama File Tidak boleh kosong");
                return;
            }
            var encode = new UnicodeEncoding();
            var content = encode.GetBytes("abcdefghijklmnopqrstuvwxyz");
            if (!File.Exists(namaFile))
            {
                using (var stream = new FileStream(namaFile, System.IO.FileMode.Create))
                {
                    await stream.WriteAsync(content, 0, content.Length);
                }
            }
            else
            {
                using (var stream = new FileStream(namaFile, System.IO.FileMode.Append))
                {
                    await stream.WriteAsync(content, 0, content.Length);
                }
            }
        }
    }
}
