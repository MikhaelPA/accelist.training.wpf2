﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfTraining2.Models
{
    public class UserNewModel
    {
        /// <summary>
        /// Default
        /// </summary>
        public string Username { get; } = Guid.NewGuid().ToString();
        public string Name { get; set; }
        public bool Gender { get; set; }
        public string Email { get; set; }
        public string Git { get; set; }
    }
}
