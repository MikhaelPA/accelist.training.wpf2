﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfTraining2.Models
{
    /// <summary>
    /// Class untuk ditampilkan ke dalam datagrid.
    /// </summary>
    public class UserViewModel
    {
        /// <summary>
        /// Default Id untuk LiteDB
        /// </summary>
        //public int Id { get; set; }

        /// <summary>
        /// Tampilin Nama user.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Email dari user
        /// Format email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Jenis kelamin
        /// Male or Female
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// Git repository user
        /// tidak wajib ada isinya
        /// </summary>
        public string Git { get; set; }
        
    }
}
