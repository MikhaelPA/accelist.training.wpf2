﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfTraining2.Models
{
    /// <summary>
    /// Model penampung appconfig
    /// </summary>
    public class AppConfigModel
    {
        /// <summary>
        /// DB Lite.
        /// </summary>
        public string LiteDb { get; set; }

        /// <summary>
        /// Nama table user.
        /// </summary>
        public string TableUser { get; set; }

        public string ApiGetUser { get; set; }

        public string ApiPostUser { get; set; }

    }
}
